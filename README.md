# T14 - Getting Started with Spark Page Rank

Denise Case 
Northwest Missouri State University
44-517 Big Data

## Getting Started

- Import this repo to your Bitbucket.
- Clone from your Bitbucket down to your local computer, e.g. C:\44564\t14.

## Install Spark on Windows

Install the following:

1. JDK 8. Install Java 8 or later. To test java installation is complete, open command prompt type java and hit enter. If you receive a message 'Java' is not recognized as an internal or external command. You need to configure your environment variables, JAVA_HOME and PATH to point to the path of jdk. (There will be several of these - more on this later.)  http://www.oracle.com/technetwork/java/javase/downloads/index.html

2. Scala. Download and install Scala. http://www.scala-lang.org/download/ 

3. Python. Install Python 2.6 or later from Python Download link. https://www.python.org/downloads/windows/

4. SBT. Download SBT. Install it with defaults. http://www.scala-sbt.org/download.html

5. Create a folder: C:\hadoop-2.7.1\bin on your laptop.

6. Download winutils.exe from:
 
- https://github.com/steveloughran/winutils/tree/master/hadoop-2.7.1/bin 
- or HortonWorks repo or http://public-repo-1.hortonworks.com/hdp-win-alpha/winutils.exe 
- or git repo at https://github.com/steveloughran/winutils/tree/master/hadoop-2.6.0/bin) 

- Place it in C:\hadoop-2.7.1\bin. Remember which one you use in case of errors.

7. Spark. Download Spark: spark-2.1.0-bin-hadoop2.7.tgz from http://d3kbcqa49mib13.cloudfront.net/spark-2.1.0-bin-hadoop2.7.tgz (a Spark pre-built package fromHadoop Spark download). Extract it. Adjust folders until you have C:\spark-2.1.0-bin-hadoop2.7\bin

## Set System Environment Variables

Set system environment variables.  

Go to Control Panel / System / Advanced System Settings / Environment variables / System Variables / New

- HADOOP_HOME       C:\hadoop-2.7.1
- JAVA_HOME         C:\Program Files\Java\jdk1.8.0_25
- SBT_HOME          C:\Program Files (x86)\sbt\
- SCALA_HOME        C:\Program Files (x86)\scala
- SPARK_HOME        C:\spark-2.1.0-bin-hadoop2.7

Use the paths that match where these files are installed on your machine (some of the paths and associated versions may be different).

## Append to System Path

Update the Path environment variable.

Go to Control Panel / System / Advanced System Settings / Environment variables / Edit / System Path variable

Add additional path entries at the end. Separate entries with a semicolon.

```DOS
;%HADOOP_HOME%\bin
;%SCALA_HOME%\bin
;%SPARK_HOME%\bin
;%SBT_HOME%\bin
```

## Test Spark

1. In Windows File Explorer, right-click on your t14 folder and "Open PowerShell Here as Admin". 
2. Type spark-shell and hit enter.
3. Exit with CTRL-C, CTRL-C or by closing the window. 
4. Verify a c:\tmp\hive folder exists.
5. You may need to delete just the hive subfolder.


## Run Spark Shell for interactive testing

1. In Windows File Explorer, right-click on your h08 folder and "Open PowerShell Here as Admin". 
1. Type spark-shell and hit enter.
1. Open http://localhost:4040/ in a browser to see the SparkContext web UI.
1. Document any errors carefully. All errors thus far are fixable - typically by making sure PATHS are correct.

## Example Data

Given a urldata.txt file with the following contents, url and outgoing url separated by a space, we can iterate to find the page ranks.

```PowerShell
url_1 url_4
url_2 url_1
url_3 url_2
url_3 url_1
url_4 url_3
url_4 url_1
```

## Run Example Scala Application

The example employs a Scala application - an object with a def main method.

```PowerShell
clear
run-example SparkPageRank .\urls.txt
```

## Example Results

You'll get an error about deleting a file, but you can see the results before the error:

```PowerShell
url_4 has rank:  1.3758228705372555 .
url_2 has rank:  0.4633039012638519 .
url_3 has rank:  0.7294952436130331 .
url_1 has rank:  1.4313779845858583 .
```

## Run Scala Script

The spark-shell includes an automatic instantiation of SparkSession as spark and SparkContext as sc.

```PowerShell
spark-shell -i script.scala
```

## Read from local text file

Follow instructions in class to create count1.py.

Open command window in this folder and run:

```PowerShell
python count1.py
```

## Read from URL

Follow instructions in class to create count2.py.

Open command window in this folder and run:

```PowerShell
python count2.py
```

## To Finish

Locate the output.

Improve python code to export only a count of unique outgoing URLs.

## Resources and References

How to add "Open Command Window Here as Administrator" to context menu:
https://www.sevenforums.com/tutorials/47415-open-command-window-here-administrator.html

How to install Spark on Windows:
http://stackoverflow.com/questions/25481325/how-to-set-up-spark-on-windows

